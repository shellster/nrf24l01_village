# First_Tutorial

This document will cover the high points of the similarly named sketch.  Before reading through this document, you should open up the associated sketch so that you can follow along.  Both sketches have been heavily commented and should be fairly self-explanatory.  If you are confused after reading this document and the comments within the sketch, please ask a village staff member for help.  Though the associated sketches can be immediately loaded on to your hardware with minimal tweaks, we highly recommend that you create your own sketch and transfer each line from the sample sketch after understanding what the line does to promote a deeper understanding of how the code works.

## Important Changes

Like the test sketches of earlier, you should update several values in the sketch before you flash it to your Arduinos:

1) The addresses array that contains the addresses for the pipes used to be unique to you.
2) The channel you wish to use (try to coordinate with others to limit overlap).
3) The `radioNumber` should be `0` for one Arduino and `1` for the other.

## Overview

This sketch is basically a slightly modified version of the RF24 library example sketch `Getting Started`. Instead of reading from USB Serial for switching to transmission, we use the dev board button on a pin.  Instead of only writing to Serial, we light up an LED.  In short, with this sketch running on both, properly configured boards, you should be able to press the button (the one not labeled reset) and have the LED on the other other board light up. You can press button on either board to light up the LED on the opposite board. This sketch demonstrates several key concepts:

1) Simple Remote Control with physical switches and real world consequences (LED lighting up).
2) The ability for both boards to send and receive at the same time (though only one button press will be registered if both buttons are simultaneously pressed).
3) Simple non-blocking Arduino programming patterns using timestamps instead of `delay`.
4) A transmit and respond with a timestamp.  This is not particularly useful or important in this sketch but the call and response will be very important in the next tutorial.


## Troubleshooting and Questions

If you are unable to follow the code or something does not make sense, please contact village staff.  This document is intentionally sparse on the details as the village is intended to be interactive.  However, if you have carefully read all the other documents in order, most of the concepts in the associated code should be relatively straight-forward.