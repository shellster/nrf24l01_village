# Software

Everything in this project is cross-OS compatible, but I recommend some sort of Linux.

For this village, you will need the Arduino IDE.  Please go install it from here: https://www.arduino.cc/en/Main/Software

Though we will not be covering it in this village, you should also check out Visual Code with the PlatformIO plugin as a cross-platform IDE alternative to the Arduino IDE. If you have experience programming Arduinos with this platform or a different one, feel free to use those, instead however please understand that village staff may be unable to assist you if problems arise.

If you have issues seeing/talking to your Arduino Nano (later in this tutorial), please talk to the village staff as they can help you check for some common issues.

We will walk through installing a couple libraries in this document.  Installing the libraries via the instructions below is preferred, however, these libraries are also available in the `Libraries` folder in this repository.

## Install RF24 Library and Configure IDE

We are going to be using the RF24 library to interact with the nRF24L01+ radios. This library is a full-featured library which is relatively easy to use and the "gold-standard" for Arduino interaction with these radio components.

Each code project that you upload or `flash` to the Arduino Nano is called a `sketch` in the Arduino IDE parlance.

To install the library, open up your Arduino IDE.  Then go to `Sketch->Include Library->Manage Libraries...`:

![](Images/menu_manage_library.png)

Now search for `RF24`, then click `Install` next to the library:

![](Images/install_rf24.png)

We recommend that you open the libraries documentation page and refer to it throughout this village: http://tmrh20.github.io/RF24/classRF24.html

Now that you have the library installed, let's test your hardware and software configuration to make sure everything is working correctly.

To do this, connect one of your Arduino Nano's via USB to your computer. Go to `Tools -> Board: "Whatever" -> Arduino Nano`:

![](Images/menu_select_board.png)

Please note that the processor is set to `ATmega328P` in the above picture. Please make sure your Processor option is similarly set. This is, most likely, the correct setting. However, if you are unable to upload to your Arduino, you may need to try other options, like `ATmega328P (Old Bootloader)`. This setting depends on which microcontroler and bootloader are used within your Arduino Nano (or clone thereof).

Now we are going to test the nRF24L01+ and the associated wiring to make sure it is correctly installed. Please re-read the "WARNING!" section in the [Hardware README](Hardware_README.md) before plugging in your circuit. Once you have re-read that section, and addressed any concerns with your wiring, plug one of your circuits into the computer via USB.

You need to make sure that the port is set correctly in the IDE. Please go to `Tools -> Port` and select your Arduino device. If you do not see an option, please check to make sure your Arduino Nano is connect to your computer via USB, and that there is a red light visible on the board. If you are unable to figure out why you cannot see your Nano, please ask village staff for assistance.  On Linux or Mac, you should see something like `/dev/ttyUSB0`. On Windows you should see an option such as `COM2`. Please note that your device is almost certainly NOT `COM0`. On Linux or OSX you may need to configure permissions to allow access for your user.  You can either Google how to do this, or contact village Staff for assistance.

With your IDE properly configured, and a working Nano plugged in, you are ready to perform a simple upload test. Please go to `File -> Examples -> 01.Basics -> Blink`: 

![](Images/menu_select_blink.png)

When the new window opens, click the little round button with an arrow in the green bar below `Edit` to upload the sketch to the Arduino Nano. You should see the words `Done uploading.` in the green bar near the bottom of the window after several seconds. If everything works correctly, you should see a little red LED light flashing on and off every second on your Nano. Repeat the same test with your second Nano before continuing. If you run into any problems, please ask the village staff for help.

## Install the SpritzCipher Library

In our second tutorial, we will be using the SpritzCipher library to secure our radio communication.  This library is almost certainly not the most efficient or secure option, however, the encryption algorithm offers reasonable security guarantees, offers both Stream Encryption and HMAC capabilities, and is extremely easy to use.  Therefore, we have chosen this library for our tutorials.  You should install this library the same way you did the `RF24` library.  Go to `Sketch->Include Library->Manage Libraries...`.  Then search for `SpritzCipher` and install the library:

![](Images/install_spritzcipher.png)

You are encouraged to review the SpritzCipher documentation and refer to it while working through the second tutorial.  You can find it here: https://github.com/abderraouf-adjal/ArduinoSpritzCipher

## Your First Radio Communication

Now we are ready to test the nRF24L01+ radios and your associated circuit. Please open the Arduino IDE then click `File -> Examples -> RF24 -> GettingStarted`:

![](Images/menu_select_rf24.png)

Before we upload this sketch to one of your circuits we need to change three things:

1) Replace the line which says `RF24 radio(7,8);` with `RF24 radio(9,10);`. This is necessary because we are using pins 9 and 10 instead of 7 and 8 for `Chip Enable` and `Chip Select Not`. For purposes of this village, it is not necessary to understand what these pins do however you can find out more about these pins in the previously linked RF24 library documentation or by perusing the [nRF24L01+ data sheet](DataSheets/nRF24L01+.pdf)
2) Update the two strings in the line which reads: `byte addresses[][6] = {"1Node","2Node"};` to two, unique 5 byte strings. **These strings should only differ on the first byte.** These strings must be unique to prevent interference with other users and their radio transmissions (unless you set a different channel, but we won't cover that right now). More information on what exactly these are will be provided later in this village, but for now, make sure that both strings are unique from each other, and unique to others at the conference.  For instance, you could pick your initials and prepend digits so that there is no overlap and each string is five characters (differing only on the first character).
3) At this point you can flash your first Arduino with the example code. However, before flashing the second one, you need to make one final change, set the line which reads `bool radioNumber = 0;` to `bool radioNumber = 1;`. This number is used by the sample code to establish which channel is for reading versus writing so that the radio's can talk to each other.

Below is a screenshot of the relevant section and changes in the `GettingStarted` sketch:

![](Images/updated_sample_sketch.png)

At this point, you should have both Arduinos programmed in fully functional circuits. Both Arduino circuits should be powered on at this point. Now, in the Arduino IDE, go to `Tools -> Serial Monitor`. Make sure that the drop-down at the bottom on the window says `115200 baud`. After a couple of seconds, you should see the following text:

![](Images/sample_sketch_serial_output1.png)

Type `T` in the box and then click `Send` or hit `Enter`. You should start to see output that looks similar to this:

![](Images/sample_sketch_serial_output2.png)

This output signifies that the two circuits are exchanging information back and forth and that everything is working correctly.  If this is not the case, please contact village staff to help diagnose the problem.

## Getting to Know Your Radio

This section will cover some basic concepts of your radio and how it works. First, you should understand that your radio only works in half-duplex mode. This means that the radio can either send or receive at any given time, but never both at the same time. By using some clever software trickery (rapid-switching, acknowledgments, and retransmissions), we can surmount this limitation and create code whereby two devices can send and receive "at the same time". Luckily, the RF24 library handles most of the complexity for us.

### Channels

Your nRF24L01+ is a 2.4Ghz radio. It can transmit and receive in the range between 2.400 to 2.525 Ghz.  This space is broken down into `channels` which are 1Mhz apart. So if you select channel 0 your nRF24L01+ will listen or transmit on 2400Mhz exactly. There are 125 channels. If you do not explicitly set a channel, the RF24 library will use channel 76 by default.

### Pipes

Pipes are logical channels inside a given physical channel.  The nRF24L01+ uses clever encoding and data masking with CRC checks to receive data from up to six `pipes` on the same channel. Though technically the radio can only ever receive one message on one pipe at one time, for all practical purposes, you can receive information on six pipes at the same time. This is achieved by auto-retransmission and acknowledgment that is done by the radio. 

Pipes are assigned 5 byte addresses (This size is changeable, but the max and default value is 5 bytes and we will not go into further detail in this village). Pipes can either be `reading pipes` or `writing pipes` ***they can never be both simultaneously***, though you can theoretically change the state of a pipe at any time (as long as the other radio is in agreement). Due to a physical requirement of the nRF24L01+, ***each pipe can only differ from each other by the first byte in the five byte address***. This last point is extremely important to grasp as it is commonly a source of confusion.

It is possible to have multiple radios transmitting and receiving on the same pipes. However, if you do this, you will need to disable automatic acknowledgment and handle collisions and retransmissions yourself. This is only needed if you need to handle more than six radios in a N-to-N configuration at the same time. This is clearly a very advanced situation, and it will not be covered in this village.

### Transmission Power Levels

For all sketches in this village, the power will be set to `RF24_PA_MIN` (-18dBm). Please do not change it during the conference.  At this transmission level the radios will have difficulty communicating more than about 10 feet away (maybe longer with an antenna), this is to reduce RF noise pollution. However for real-world use, you will likely want to replace `RF24_PA_MIN` in your sketch with `RF24_PA_Max` (0dBm). This will greatly boost your transmission range. Since you will usually want to configure your radios to auto-acknowledge messages, both the sender and receiver should be set to the same power levels.

### Data Transmission Levels

The nRF24L01+ can transmit up to 2MBps, however at this rate you will lose significant distance and your transmissions can bleed into neighboring channels. By default, the radio is configured to transmit at a rate of 1MBps by default. Some variants ("+" or "p" variants, which you likely have) can be lowered to 250KBps for maximum range, but this setting is not compatible across all variants. Ask a village staff member if you would like more information on how to test this.

### Data Transmission Length Limits

The nRF24L01+ can transmit up to 32 Bytes of data in a single transmission. For longer transmissions, you must design your communication protocol to handle automatic packet splitting and reassembly.

For purposes of this village, we will not handle these more complex situations, but the length limitation will become important in the second tutorial. 

By default, the radio expects that all packets will be the same length.  Therefore, the radio will collect data until it gets the expected amount of data, wait for that data to be read by the program, then collect more data.  A shorter or longer transmission will be considered erroneous. It is possible to send dynamically sized packets, but this will also not be covered within this village, just be aware that for this village, your sent data should always be the same length.

### ACKs, Retries, CRCs

The most painful thing about most radio communication is handling failures, corruptions, and retransmissions. What makes the nRF24L01+ so much easier to use than other, cheaper 2.4GHz transceiver modules is that it handles most of this painful stuff for you in a transparent manner. By default, the radio will automatically handle sending an ACK on every packet received. It will check a 16bit CRC on each packet to ensure that it was not corrupted, and in the event of failure or corruption, it will retry up to 5 times, with a 15 millisecond delay between each retry.  All of this can be controlled by you, but you do not need to adjust the defaults in most cases. Adjusting these settings is outside the scope of this village and most-likely, your project needs.