# Hardware

For this project we will be using an Arduino Nano microcontroller board for the brains of our circuit, and we will use the nRF24L01+ for the radio to perform wireless communication.

## Overview

Below are graphics showing the pinouts of the Arduino Nano Clones and the nRF24L01+.
When wiring up these components, it is important that you understand what you are doing.
The Arduino Nano is communicating with the nRF24L01+ via SPI.  Therefore, for maximum efficiency, you MUST use the SPI dedicated pins on the Nano (MISO, MOSI, SCK). In addition we are going to use two general purpose pins (D10 and D9) for "Chip Enable" (CE) and "Chip Select Not" (CSN) 

Additional Pins that we will wire up, include Voltage at the Common Collector (VCC) 3.3V and Ground (GND).

Finally we will use D2 to power and LED and D3 to register button presses.

Below is a pinout of the Arduino Nano, courtesy of ProjectIoT123.com (which has lots of good information and projects):

![Nano](Images/arduino-nano-pinout.jpg)

Below is a pinout for the nRF24L01+, courtesty of TheEngineeringProjects.com another great source of electronic schematic and circuit knowledge:

![NRF24l01](Images/nRF24L01-pinout.jpg)

Finally, you can find a Fritzing schematic of how to wire these two devices together in the image below:

![Schematic](Images/schematic.png)

The white wires represent ground lines that you will not need to wire as they are already built into the project board, you only need to connect ground on the Nano to one of the board ground references.  Please look at the two sample boards for more specific information. 

If you are not familiar with Fritizing, I highly recommend that you download it (http://fritzing.org/download/0.9.3b/) and import the project which is located here: [Fritizing Project](Fritzing/nRF24L01Sketch.fzz). This is helpful because you can see where each wire goes on both components.

**Additional Resources***

*) [Nordic nRF24L01++ DataSheet](DataSheets/nRF24L01+.pdf)

*) [Arduiono Nano User Manual](DataSheets/NanoV3.pdf)

## Instructions

You are going to need to wire up two boards so that you can send and receive information.
One of the most important things to note is the inclusion of a 10uf Electrolytic capacitor between Vin and Ground on the nRF24L01+. Due to an arguable design flaw in the chip this is necessary in most cases for the radio to work correctly. The necessary inclusion of this capacitor is one of the biggest gotchas when working with these radios. Apparently, some genuine versions of the nRF24L01+ do not require this capacitor, however it is exceedingly difficult to determine whether your radio is genuine or a Chinese knock-off, and even some genuine versions may require the addition of the capacitor to function stability.

**CAUTION** 

Electrolytic capacitors **must be correctly aligned** with regard to polarity or you risk a minor explosion.

### WARNING! 

Incorrectly wiring your device up could result in one or more components being destroyed.  We will have limit or no replacement parts, so it is very important to ensure that your circuit is wired correctly, as you will likely not be able to recover from hardware failure.  It is very important to carefully examine your circuit before power is applied. Use a continuity tester (multimeters at the event have this functionality) to ensure that there is continuity between each solder or wire connection and the connection on the other end of the wire. Also use the continuity tester to ensure that there is **NO** continuity between nearby soldered connections, as this is the most likely point of failure. Finally be careful of common ground and power rails and make sure that none of your solder joins inadvertently bridge to one of these. A good idea is to use the continuity to test that there is no continuity between the common GND and VCC rails and any points in the circuit that should not be tied to those rails. If you are uncertain or if you need help correcting a mistake, please ask for assistance from the village staff. Finally, once you apply power to your circuit, please carefully watch the circuit for the first couple minutes. Look for smoke, smell for burning components, and carefully touch the various components. If any of the components are getting excessively warm to the touch, **Immediately disconnect your circuit** and inspect it for bridged connections, or wiring mistakes.