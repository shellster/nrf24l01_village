# Quick Arduino Coding Overview

This document provides a quick overview of what "Arduino Programming" is.  Feel free to skip this document if you are already familar with programming within the Arduino ecosystem.

The Arduino IDE Environment simply uses the C++ language with a collection of helpful libraries and macros to make things easier.  This village will not cover C++ basics as this is beyond the scope of this village.  If you have done any programming in the past, you will likely be able to follow along with the programming tutorials.

Each Arduino project or `sketch` requires the following two methods (you can of course define additional methods as well): `void setup()` and `void loop()`.  The setup method is called once, each time the Arduino board is powered up. The setup method is always called first on power-up. After that, the loop method is called repeatedly until the Arduino is powered off. 

The Arduino website is an excellent resource for Arduino helper functions and macros.  Due to time limitations, we will not cover Arduino programming in depth.  We will instead only briefly cover some of the methods we are using.  For more details please visit the [Arduino Website](https://www.arduino.cc/reference/en/).  

## Pin Modes and Settings

Every Arduino has numbered pins. These pins can be used for a number of special purposes, or for "General Purpose Input and Output" (GPIO). Some of the special use cases include pins that are designed for serial communication (such as the ones we are using to talk to the nRF24L01+ radios).  To use pins for GPIO, you must set their mode. You do this by using the `pinMode` method. If you want to set the voltage of pin `12`. You would need to set the pin to `output` mode like so: `pinMode(12, OUTPUT);`

For reading input from a pin, you must set the pin into an input mode. There are two types of input: `INPUT` and `INPUT_PULLUP`.  `INPUT` will pull the pin to `GND` by default.  `INPUT_PULLUP` will pull the pin to the `VCC` voltage by default. In out sketches we will be setting our Button-attached pin to `INPUT_PULLUP` as the dev boards automatically tie one side of the switch to ground.  Therefore, to test if the switch is pressed, we need thhe voltage of the pin to normally be at `VCC`.  Now is a good time to point out that Arduinos allow us to take several shortcuts that are normally not possible on small micro-controllers. Proper Arduinos have built in resistors to limit current on pins to 40 millamps. Depending on whether a pin is set to INPUT or INPUT_PULLUP, they also have resistors to pull pins to `GND` or `VCC` to prevent erronous readings. Most microcontrollers would require additional resistors in our circuit to address too much current or so-called `floating` pins.  Luckily neither of these issues need concern us today.

For more information on `pinMode` see: https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/

To read whether a pin is tied to `VCC` or `GND` we can use `digitalRead`, like `if(digitalRead(12) == HIGH)`.  `HIGH` and `LOW` are built in defines for `VCC` and `GND`. There is also an `analogRead` for pins that support this functionality, but we will not discuss it further in this document. To set an output pin to `HIGH` or `LOW`, we will use the `digitalWrite` method.  For example: `digitalWrite(12, HIGH);`. There is also an `analogWrite` method which will use `Pulse Width Modulation` (PWM) on most pins to achieve a specific voltage, or a true specific voltage if your Arduino pin supports it. We will not be using `analogWrite` in this village.

For more information on `digitalRead`: https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/

For more information on `digitalWrite`:
https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/

## Serial Communication

Serial communication is the most useful debugging technique provided by the Arduino IDE. We will use it for control and debugging within the village sketches.
To start using Serial communication, you must call `Serial.begin()`.  You can set the speed for serial communication by passing it as an argument to the `begin` method like: `Serial.begin(115200);`.  Some Arduino boards will delay further execution until Serial connection is established.

To read from the Serial interface you can call `Serial.readString`, `Serial.readBytes`, or `Serial.read`.  For purposes of brevity, we will not cover these methods in more detail.

To write to the Serial Monitor, you can use `Serial.print` and `Serial.println`.  `Serial.println` adds a line ending to the end of your line. The village sketches use the `F()` macro which forces the string into program memory instead of RAM. This is generally a good thing as most Arduino environments have very little RAM.  

More information on the `Serial` class can be found here: https://www.arduino.cc/reference/en/language/functions/communication/serial/

## EEPROM Library

The final "built-in" library we will cover is the the EEPROM library which allows you to save and load data to EEPROM memory which survives poweroff/poweron cycles. In our second village sketch, we will be using an incremented counter.  This counter's value must be stored and restored between power-on cycles, so we will use the EEPROM library.  The EEPROM library has a series of relatively primative methods for reading and storing data within the Arduino's EEPROM chip.  We will only use two of these methods: `EEPROM.get` and `EEPROM.put`.  

To get a stored value of a certain type out of EEPROM, declare the variable to store the data, then pass the location, and the variable to `EEPROM.get` like this:
```
unsigned long counter;
EEPROM.get(0, counter);
```

To store a value into EEPROM, call `EEPROM.put` with the location where you wish to store the value, and the value, like this: `EEPROM.put(0, counter);`

More information on the various methods in this class can be found here: https://www.arduino.cc/en/Reference/EEPROM
