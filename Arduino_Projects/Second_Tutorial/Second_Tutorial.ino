/*************************************************************************************
 * Second Tutorial '
 * nRF24L01+ Village
 * BSidesBoise 2019
 * 
 * Originally Written by Shelby Spencer (With heavy inspiration from RF24 Getting Started sketch)
 */

#include <SPI.h>
#include <EEPROM.h>
#include <RF24.h>
#include <SpritzCipher.h>

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/
bool radioNumber = 1;

#define LED 2  //Define LED to match the pin wired to our light
#define BUTTON 3  //Define BUTTON to match the pin wired to our light

#define COUNTERGAP 3 //How far apart the counters can be


// Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 8 & 9 
RF24 radio(9,10);

/**********************************************************/

const byte addresses[][6] = {"12ABA","22ABA"};

const byte key[6] = { 0x00, 0x01, 0x02,  0x03, 0x04, 0x05 }; //Encryption Key

// Used to control whether this node is sending or receiving
bool role = 0;

// Holds the time the light has turned on
unsigned long blink_time; 

// Holds the time to track button press length
unsigned long transmit_time; 

typedef struct {
    unsigned long counter; // Counter to prevent replay attacks
    byte hmac[28]; // HMAC limited to 28 bytes so total packet is 32 bytes  max packet size (radio limitation);
} Packet;

unsigned long counter; // Global to hold counter

Packet tpacket; // Packet for transmitting
Packet rpacket; // Packet for holding received data

bool updated = false; // Track if the other radio sent us a correction. 


// Utility function for converting unsigned long to byte array for Spritz
void ulong2barray(unsigned long number, byte * byteArray) {
    byteArray[0] = (int)((number >> 24) & 0xFF) ;
    byteArray[1] = (int)((number >> 16) & 0xFF) ;
    byteArray[2] = (int)((number >> 8) & 0XFF);
    byteArray[3] = (int)((number & 0XFF));
}

// Utility function to print out Spritz HMAC
void printHMAC(byte * hmac, int len) 
{
    for (int i = 0; i < len; i++) 
    {
        if (hmac[i] < 0x10) // To print "0F", not "F"
        {
            Serial.write('0');
        }
        
        Serial.print(hmac[i], HEX);
    }
}

void setup() 
{
    EEPROM.get(0, counter); //load stored counter from EEPROM;
    
    pinMode(LED, OUTPUT);  //Assign LED pin to be in output mode
  
    pinMode(BUTTON, INPUT_PULLUP);  //Assign Button to be in INPUT_PULLUP mode since we have it tied to ground when pushed.
  
    Serial.begin(115200);
  
    radio.begin();
    
    radio.setChannel(1); // Set channel 0 to 125 (you may want to adjust yours)
  
    // Set the PA Level low to prevent power supply and radio pollution related issues since this is a
    // workshop sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
    radio.setPALevel(RF24_PA_MIN);
  
    // Open a writing and reading pipe on each radio, with opposite addresses
    if(radioNumber) 
    {
        radio.openWritingPipe(addresses[1]);
        radio.openReadingPipe(1,addresses[0]);
    } 
    else 
    {
        radio.openWritingPipe(addresses[0]);
        radio.openReadingPipe(1,addresses[1]);
    }
  
    // Start the radio listening for data
    radio.startListening();
}

void loop()
{
    byte tempHmac[28];  // Will hold computerd HMAC for received packets
    byte tempByteArray[4];  // Will hold counter bytes for Spritz HMAC function
    
    /****************** Ping Out Role ***************************/
    
    if(digitalRead(BUTTON) == LOW && (micros() - transmit_time > 100000)) //When button is pressed
    {  
        transmit_time = micros();
        
        counter++;

        ulong2barray(counter, tempByteArray);  // Convert counter to byte array so Spritz can use
        spritz_mac(tpacket.hmac, (uint8_t) sizeof(tpacket.hmac), tempByteArray, (uint8_t) sizeof(tempByteArray), key, (uint8_t) sizeof(key)); // HMAC counter and store in tpacket.hmac
        tpacket.counter = counter; // Assign counter to tpacket.counter

        Serial.println(F("Transmitting"));
        Serial.print(F("Now sending: \n\tCounter: "));
        Serial.println(tpacket.counter);
        Serial.print(F("\tHMAC: "));
        printHMAC(tpacket.hmac, sizeof(tpacket.hmac));
        Serial.println(F(""));

        unsigned long start_time = micros();

        radio.stopListening();  // First, stop listening so we can talk.
    
        if (!radio.write(&tpacket, sizeof(tpacket)))
        {
            counter--;  // We failed to transmit so decrement counter back to last sent value
             
            transmit_time = 0;
            Serial.println(F("failed"));
            return;
        }
        
        radio.startListening();  // Now, continue listening
    
        unsigned long started_waiting_at = micros();  // Set up a timeout period, get the current microseconds
        boolean timeout = false;  // Set up a variable to indicate if a response was received or not
    
        while (!radio.available())  // While nothing is received
        { 
            if (micros() - started_waiting_at > 200000)  // If waited longer than 200ms, indicate timeout and exit while loop
            { 
                timeout = true;
                break;
            }      
        }
        
        if (timeout) // We didn't get a response in 200ms, so consider it a failure reset transmit_time and decrement counter to previous value
        {
            counter--;
            transmit_time = 0;
            Serial.println(F("Failed, response timed out."));
        }
        else
        {
            // Grab the response, compare, and send to debugging spew
            radio.read(&rpacket, sizeof(rpacket));
            
            Serial.print(F("Sent Packet:\n\tCounter: "));
            Serial.println(tpacket.counter);
            Serial.print(F("\tHMAC: "));
            printHMAC(tpacket.hmac, sizeof(tpacket.hmac));
            Serial.print(F(", Got Packet:\n\tCounter: "));
            Serial.print(rpacket.counter);
            Serial.print(F("\tHMAC: "));
            printHMAC(rpacket.hmac, sizeof(rpacket.hmac));
            Serial.println(F(""));
            Serial.print(F(", Round-trip delay "));
            Serial.print(micros()-start_time);
            Serial.println(F(" microseconds"));

            // Recalculate the HMAC for the received packet
            ulong2barray(rpacket.counter, tempByteArray);
            spritz_mac(tempHmac, (uint8_t) sizeof(tempHmac), tempByteArray, (uint8_t) sizeof(tempByteArray), key, (uint8_t) sizeof(key));

            // Compare received packet HMAC with the expected value and reject if they do not match
            if(spritz_compare(tempHmac, rpacket.hmac, sizeof(tempHmac)) == 0)
            {
                if(rpacket.counter == tpacket.counter) // Receiver responded and agreed with our counter value
                {
                    updated = true;  // Set updated so that the a "receiver" can't replay a message in the future to reset counter
                    Serial.println(F("Counters Match!"));
                }
                else
                {
                    Serial.println(F("Counters Do Not Match!"));
                    
                    if(updated)
                    {
                        Serial.println(F("Already updated, someone may be trying a replay attack"));
                    }
                    else
                    {
                        updated = true;  // Set updated so that the a "receiver" can't replay a message in the future to reset counter
                        Serial.println(F("Assigning returned value"));
                        counter = rpacket.counter;
                    }
                }

                EEPROM.put(0, counter);
            }
            else
            {
                // Received Packet had invalid HMAC
                Serial.println("Failed HMAC! Packet Rejected!");
                counter--;
            }
        }
    }
    else
    {
        /****************** Pong Back Role ***************************/
        if(radio.available())  // Variable for the received timestamp
        {
            while (radio.available())  // While there is data ready
            {                                   
                radio.read(&rpacket, sizeof(rpacket));  // Get Packet

                // Recalculate the HMAC for the received packet
                ulong2barray(rpacket.counter, tempByteArray);
                spritz_mac(tempHmac, (uint8_t) sizeof(tempHmac), tempByteArray, (uint8_t) sizeof(tempByteArray), key, (uint8_t) sizeof(key));

                // Compare received packet HMAC with the expected value and reject if they do not match
                if(spritz_compare(tempHmac, rpacket.hmac, sizeof(tempHmac)) == 0)
                {
    
                    Serial.print(F(", Got Packet:\n\tCounter: "));
                    Serial.print(rpacket.counter);
                    Serial.print(F("\tHMAC: "));
                    printHMAC(rpacket.hmac, sizeof(rpacket.hmac));
                    Serial.println(F(""));       

                    // Check if the counter is near what we expected
                    // Make sure that the received packet is greater than ours
                    if((rpacket.counter > counter) && (rpacket.counter - counter <= COUNTERGAP))
                    {
                        // Set our counter to match the transmitted one and store it.
                        counter = rpacket.counter; 
                        EEPROM.put(0, counter);
    
                        radio.stopListening();  // First, stop listening so we can talk   
                        radio.write(&rpacket, sizeof(rpacket));  // Echo back packet.
                        radio.startListening();  // Now, resume listening so we catch the next packets.
                        
                        Serial.print(F("Packet in acceptable range."));
                        Serial.println(F("Turn on light"));
                        
                        blink_time = micros();    
                        digitalWrite(LED, HIGH);
                    }
                    else
                    {
                        // Create a transmit packet containing our counter
                        ulong2barray(counter, tempByteArray);
                        spritz_mac(tpacket.hmac, (uint8_t) sizeof(tpacket.hmac), tempByteArray, (uint8_t) sizeof(tempByteArray), key, (uint8_t) sizeof(key));
                        tpacket.counter = counter;

                        // Send our version of reality so that Transmitter can sync up
                        radio.stopListening();  // First, stop listening so we can talk   
                        radio.write(&tpacket, sizeof(tpacket));  // Send expected counter value back.  
                        radio.startListening();  // Now, resume listening so we catch the next packets.
    
                        Serial.println("Packet failed checks!");
                        Serial.print(F("Sending Back: \n\tCounter: "));
                        Serial.println(tpacket.counter);
                        Serial.print(F("\tHMAC: "));
                        printHMAC(tpacket.hmac, sizeof(tpacket.hmac));
                        Serial.println(F(""));
                    }
                }
                else
                {   
                    // Received packet had invalid HMAC
                    Serial.println("Failed HMAC! Packet Rejected!");
                }
            }
        }
    }

    if(micros() - blink_time > 1000000)
    {
        digitalWrite(LED, LOW);
    }
} // Loop
