/*************************************************************************************
 * First Tutorial '
 * nRF24L01+ Village
 * BSidesBoise 2019
 * 
 * Originally Written by Shelby Spencer (With heavy inspiration from RF24 Getting Started sketch)
 *************************************************************************************/

#include <SPI.h>
#include "RF24.h"

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/
bool radioNumber = 0;

#define LED 2  //Define LED to match the pin wired to our light
#define BUTTON 3  //Define BUTTON to match the pin wired to our light

/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 8 & 9 */
RF24 radio(9,10);
/**********************************************************/

byte addresses[][6] = {"12ABA","22ABA"};

// Used to control whether this node is sending or receiving
bool role = 0;
//Holds the time the light has turned on
unsigned long blink_time;  

void setup() 
{
    pinMode(LED, OUTPUT);  //Assign LED pin to be in output mode
  
    pinMode(BUTTON, INPUT_PULLUP);  //Assign Button to be in INPUT_PULLUP mode since we have it tied to ground when pushed.
  
    Serial.begin(115200);
  
    radio.begin();
    
    radio.setChannel(1); // Set channel 0 to 125 (you may want to adjust yours)
    
    // Set the PA Level low to prevent power supply and radio pollution related issues since this is a
    // workshop sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
    radio.setPALevel(RF24_PA_MIN);
  
    // Open a writing and reading pipe on each radio, with opposite addresses
    if(radioNumber) 
    {
        radio.openWritingPipe(addresses[1]);
        radio.openReadingPipe(1,addresses[0]);
    } 
    else 
    {
        radio.openWritingPipe(addresses[0]);
        radio.openReadingPipe(1,addresses[1]);
    }
  
    // Start the radio listening for data
    radio.startListening();
}

void loop()
{
    /****************** Ping Out Role ***************************/
    if(digitalRead(BUTTON) == LOW)  //When button is pressed
    {  
        Serial.println(F("Transmitting"));
    
        radio.stopListening();  // First, stop listening so we can talk.

        Serial.println(F("Now sending"));

        unsigned long start_time = micros();  // Take the time, and send it.  This will block until complete
    
        if (!radio.write( &start_time, sizeof(unsigned long)))
        {
           Serial.println(F("failed"));
        }
        
        radio.startListening();  // Now, continue listening
    
        unsigned long started_waiting_at = micros();  // Set up a timeout period, get the current microseconds
        boolean timeout = false;  // Set up a variable to indicate if a response was received or not
    
        while (!radio.available())  // While nothing is received
        { 
            if (micros() - started_waiting_at > 200000)  // If waited longer than 200ms, indicate timeout and exit while loop
            { 
                timeout = true;
                break;
            }      
        }
        
        if (timeout)
        {
            Serial.println(F("Failed, response timed out."));
        }
        else
        {
            // Grab the response, compare, and send to debugging spew
            unsigned long got_time;  
            radio.read(&got_time, sizeof(unsigned long));
            unsigned long end_time = micros();
        
            Serial.print(F("Sent "));
            Serial.print(start_time);
            Serial.print(F(", Got response "));
            Serial.print(got_time);
            Serial.print(F(", Round-trip delay "));
            Serial.print(end_time-start_time);
            Serial.println(F(" microseconds"));
        }
    }
    else
    {
        /****************** Pong Back Role ***************************/
        unsigned long got_time;
    
        if(radio.available())  // Variable for the received timestamp
        {
            while (radio.available())  // While there is data ready
            {                                   
                radio.read(&got_time, sizeof(unsigned long));  // Get the payload
            }
     
            radio.stopListening();  // First, stop listening so we can talk   
            radio.write(&got_time, sizeof(unsigned long));  // Send the final one back.      
            radio.startListening();  // Now, resume listening so we catch the next packets.     
     
            Serial.print(F("Sent response "));
            Serial.println(got_time);  

            blink_time = micros();    

            Serial.println(F("Turn on light"));
            digitalWrite(LED, HIGH);
        }
    }

    if(micros() - blink_time > 1000000)
    {
        digitalWrite(LED, LOW);
    }
} // Loop
