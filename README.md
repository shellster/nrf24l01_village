# nRF24L01+ Village

This material was taught at BSidesBoise 2019 by Shelby Spencer (shellster).

This information is freely available for all to use.  If you see error (spelling, grammar, or technical) or you wish to contribute more material, please open an issue (ideally with a pull request).

# Instructions

Please start by reading the Markdown files under Documentation in-order (numerically), starting with [1. Hardware](Documentation/1_Hardware_README.md).